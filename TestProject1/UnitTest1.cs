using System.Threading.Tasks;
using NUnit.Framework;
using TauZ2.Services;

namespace TestProject1;

public class Tests
{
    private ExampleService _exampleService;
    
    [SetUp]
    public void Setup()
    {
        _exampleService = new ExampleService();
    }

    [Test]
    public void Test1()
    {
        var serviceResult =  _exampleService.Add(10, 10).Result;
        Assert.That(serviceResult, Is.EqualTo(20));
    }
    
    [Test]
    public async Task AddAsync_Returns_The_Sum_Of_X_And_Y()
    {
        int result = await _exampleService.AddAsync(1, 1);
        Assert.AreEqual(2, result);
    }
    
    [Test]
    public void IsNull() {
        object obj = null;
        Assert.That(obj, Is.Null);
    }

    [Test]
    public void IsNotNull() {
        Assert.That(42, Is.Not.Null);
    }

    [Test]
    public void IsTrue() {
        Assert.That(2 + 2 == 4, Is.True);
    }

    [Test]
    public void IsFalse() {
        Assert.That(2 + 2 == 5, Is.False);
    }

    [Test]
    public void IsNaN() {
        double d = double.NaN;
        Assert.That(d, Is.NaN);
    }

    [Test]
    public void CollectionContainsTests() {
        int[] iarray = new int[] {
            1, 2, 3
        };

        Assert.That(iarray, Has.Member(3));
        Assert.That(iarray, Has.All.LessThan(10));
    }
    
}