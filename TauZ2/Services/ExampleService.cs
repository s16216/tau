﻿namespace TauZ2.Services;

public class ExampleService
{
    public Task<int> Add(int a, int b)
    {
        return Task.FromResult(a + b);
    }
    
    public async Task<int> AddAsync(int x, int y)
    {
        await Task.Delay(100).ConfigureAwait(false);
        
        return (x + y);
    }
}